import React from 'react';
import MainContainer from './components/MainContainer/MainContainer';
import { GlobalStyle } from './assets/styles/GlobalStyle';
import { createStore } from 'redux';
import { Provider } from 'react-redux'

const initialState = {
  name: '',
  time: '',
  dish: '',
  noOfSlices: '',
  diameter: '',
  spicinessScale: 5,
  slicesOfBread: '',
  submit: false
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SAVE_NAME':
      return {
        ...state,
        name: action.payload
      }
    case 'SAVE_PREPARATION_TIME':
      return {
        ...state,
        time: action.payload
      }
    case 'SAVE_DISH':
      return {
        ...state,
        dish: action.payload
      }
    case 'SAVE_NO_OF_SLICES':
      return {
        ...state,
        noOfSlices: action.payload
      }
    case 'SAVE_DIAMETER':
      return {
        ...state,
        diameter: action.payload
      }
    case 'SAVE_SPICINESS_SCALE':
      return {
        ...state,
        spicinessScale: action.payload
      }
    case 'SAVE_SLICES_OF_BREAD':
      return {
        ...state,
        slicesOfBread: action.payload
      }
    case 'SET_SUBMIT':
      return {
        ...state,
        submit: action.payload
      }
    default:
      return state;
  }
}

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

function App() {
  return (
    <Provider store={store}>
      <GlobalStyle />
      <MainContainer />
    </Provider>
  );
}

export default App;
