import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
    body {
        padding: 0;
        margin: 0;
        font-family: 'Montserrat', sans-serif;
        font-size: 16px;
        font-size: 1.6rem;
        line-height: 1.5;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    *, *::before, *::after {
        box-sizing: border-box;
    }
`