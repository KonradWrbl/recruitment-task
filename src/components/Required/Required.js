import React from 'react';
import { StyledSpan } from './styles';

const Required = () => (
    <StyledSpan>*required</StyledSpan>
)

export default Required