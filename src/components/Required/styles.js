import styled from 'styled-components'

export const StyledSpan = styled.span`
    font-size: .4em;
`