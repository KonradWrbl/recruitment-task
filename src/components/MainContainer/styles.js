import styled from 'styled-components'

export const StyledWrapper = styled.div`
    width: 100vw;
    height: 100VH;
    padding: 10px 10px 30px 10px;
    background-color: #F2F2F2;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

export const StyledForm = styled.form`
    width: 90%;
    max-width: 700px;
`

export const H1 = styled.h1`
    font-size: 1.5rem
`