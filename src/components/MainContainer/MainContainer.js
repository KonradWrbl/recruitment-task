import React, { useState, useEffect }from 'react';
import { StyledWrapper, StyledForm, H1 } from './styles';
import Input from '../Input/Input';
import SpecialInput from '../SpecialInput/SpecialInput';
import { connect } from 'react-redux'
import { setSubmit } from '../../redux/actions/actionCreators';

const MainContainer = props => {

    const [submit, setSub] = useState(false)

    const { name, time, dish, noOfSlices, diameter, spicinessScale, slicesOfBread } = props;

    const handleSubmit = e => {
        let sendingData = {}

        setSub(true)
        e.preventDefault()


        //conditions to send the post
        if ((name === '') || (time === '') || (dish === '') || (dish === '--')) return
        else if ((dish === 'pizza') && ((diameter === '') || (noOfSlices === ''))) return
        else if ((dish === 'sandwich') && (slicesOfBread === '')) return

        //post data structure depends on selected dish
        switch (dish) {
            case 'pizza':
                sendingData = {
                    name: name,
                    preparation_time: time,
                    type: dish,
                    diameter: diameter,
                    no_of_slices: noOfSlices,
                    id: 0
                }
                break
            case 'soup':
                sendingData = {
                    name: name,
                    preparation_time: time,
                    type: dish,
                    spiciness_scale: spicinessScale,
                    id: 0
                }
                break
            case 'sandwich':
                sendingData = {
                    name: name,
                    preparation_time: time,
                    type: dish,
                    no_of_slices: slicesOfBread,
                    id: 0
                }
                break
            default:
                break
        }

        console.log(JSON.stringify(sendingData));

        fetch('https://frosty-wood-6558.getsandbox.com:443/dishes', {  //POST message using fetch
            method: 'post',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify(sendingData),
        })
        .then(response => {
            console.log(response);
            alert('Your order has been placed')
        })
        .catch(error => {
            console.log(error);
            alert('We cant place your order :(')
        })
    }



    useEffect(() => {               //useEffect from React Hooks
        props.setSubmit(submit)
    }, [submit])

    return(
        <StyledWrapper>
            <H1>Order your meal!</H1>
            <StyledForm onSubmit = {handleSubmit}>
                <Input containing='Name'/>
                <Input containing='Preparation Time'/>
                <Input containing='Dish'/>
                <SpecialInput dish={props.dish}/>
                <Input containing='Submit'/>
            </StyledForm>
        </StyledWrapper>
    )
}

const mapStateToProps = state => {
    return {
        name: state.name,
        time: state.time,
        dish: state.dish,
        noOfSlices: state.noOfSlices,
        diameter: state.diameter,
        spicinessScale: state.spicinessScale,
        slicesOfBread: state.slicesOfBread
    }
}

const mapDispatchToProps = {
    setSubmit: setSubmit
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)