import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { SpecialInputWrapper, StyledSpecialInputLabel, StyledSpecialInput, StyledInputStandard } from './styles';
import { saveNoOfSlices, saveDiameter, saveSpicinessScale, saveSlicesOfBread } from '../../redux/actions/actionCreators'
import Required from '../Required/Required';

const SpecialInput = props => {

    const [state, setState] = useState({
        noOfSlices: props.noOfSlices,
        diameter: props.diameter,
        spicinessScale: props.spicinessScale,
        slicesOfBread: props.slicesOfBread
    })

    const dish = props.dish

    const SpecialInputFoo = () => {  //different JSX depends on selected dish type
        switch (dish) {
            case 'pizza':
                return (
                    <>
                        <StyledSpecialInputLabel htmlFor = {`${dish}_slices`}>Number of pizza slices</StyledSpecialInputLabel>
                        <StyledInputStandard
                            id = {`${dish}_slices`}
                            type = 'number'
                            value = {state.noOfSlices}
                            onChange = {e => setState({...state, noOfSlices: e.target.value})}
                        />
                        {(props.submit && (state.noOfSlices === '')) ? <Required /> : ''}
                        <StyledSpecialInputLabel htmlFor = {`${dish}_diameter`}>Diameter</StyledSpecialInputLabel>
                        <StyledInputStandard
                            id = {`${dish}_diameter`}
                            options = {{numeral: true}}
                            value = {state.diameter}
                            onChange = {e => setState({...state, diameter: e.target.value})}
                        />
                        {(props.submit && (state.diameter === '')) ? <Required /> : ''}

                    </>
                )
            case 'soup':
                return (
                    <>
                        <StyledSpecialInputLabel htmlFor = {dish}>Spiciness Scale: {state.spicinessScale}</StyledSpecialInputLabel>
                        <StyledInputStandard
                            id = {dish}
                            type = 'range'
                            min = '1'
                            max = '10'
                            value = {state.spicinessScale}
                            onChange = {e => setState({spicinessScale: e.target.value})}
                        />
                        {(props.submit && (state.spicinessScale === '')) ? <Required /> : ''}

                    </>
                )
            case 'sandwich':
                return (
                    <>
                        <StyledSpecialInputLabel htmlFor = {dish}>Number of slices of bread</StyledSpecialInputLabel>
                        <StyledInputStandard
                            type = 'text'
                            id = {dish}
                            options = {{numeral: true}}
                            value = {state.slicesOfBread}
                            onChange = {e => setState({slicesOfBread: e.target.value})}
                        />
                        {(props.submit && (state.slicesOfBread === '')) ? <Required /> : ''}

                    </>
                )
            default:
                return;
        }
    }

    const sendData = () => {   //function to sending data to reducer

        switch (dish) {
            case 'pizza' :
                props.saveNoOfSlices(state.noOfSlices)
                props.saveDiameter(state.diameter)
                console.log('pizza!');
                break;
            case 'soup' :
                props.saveSpicinessScale(state.spicinessScale);
                console.log('soup!');
                break;
            case 'sandwich' :
                props.saveSlicesOfBread(state.slicesOfBread)
                console.log('sandwich!');
                break;
            default:
                break;
        }
    }

    useEffect(() => {
        sendData()
    }, [state.noOfSlices, state.diameter, state.spicinessScale, state.slicesOfBread])

    return(
        <SpecialInputWrapper>
            {SpecialInputFoo()}
        </SpecialInputWrapper>
    )
}

const mapStateToProps = state => {
    return {
        noOfSlices: state.noOfSlices,
        diameter: state.diameter,
        spicinessScale: state.spicinessScale,
        slicesOfBread: state.slicesOfBread,
        submit: state.submit,
    }
}

const mapDispatchToProps = {
    saveNoOfSlices,
    saveSpicinessScale,
    saveDiameter,
    saveSlicesOfBread,
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialInput)