import styled from 'styled-components'

export const StyledInputStandard = styled.input`
    height: 2em;
    width: 100%;
`

export const SpecialInputWrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 5px 0;
`

export const StyledSpecialInputLabel = styled.label`
    font-size: .5em;
`