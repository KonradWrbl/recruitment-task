import styled from 'styled-components'
import Cleave from 'cleave.js/react';

export const InputWrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 5px 0;
`

export const StyledInput = styled(Cleave)`
    height: 2em;
    width: 100%;
`

export const StyledLabel = styled.label`
    font-size: .5em;
`

export const StyledSelect = styled.select`
    height: 2em;
    width: 100%;
`