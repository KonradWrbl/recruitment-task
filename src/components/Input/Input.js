import React, { useState, useEffect } from 'react';
import { StyledInput, InputWrapper, StyledLabel, StyledSelect } from './style';
import {connect} from 'react-redux'
import { saveName, savePreparationTime, saveDish } from '../../redux/actions/actionCreators'
import Required from '../Required/Required';


const Input = props => {

    const [state, setState] = useState({ 
        name: props.name,
        time: props.time,
        dish: props.dish
    })

    const containing = props.containing

    const inputType = type => {
        switch(type) {                  //diiferent JSX depends on input name incudes in props
            case 'Preparation Time':
                return (
                    <>
                        <StyledInput
                            id = {containing}
                            placeholder={`hours : minutes : seconds`}
                            options = {{time: true, timePattern: ['h', 'm', 's']}}
                            onChange = {e => setState({time: e.target.value})}
                        />
                        {(props.submit && (state.time === '')) ? <Required /> : ''}
                    </>
                )
            case 'Dish':
                return (
                    <>
                        <StyledSelect
                            value = {state.dish}
                            onChange = {e => setState({dish: e.target.value})}
                            >
                            <option value = '--'>--</option>
                            <option value = 'pizza'>pizza</option>
                            <option value = 'soup'>soup</option>
                            <option value = 'sandwich'>sandwich</option>
                        </StyledSelect>
                        {(props.submit && ((state.dish === '') || (state.dish === '--'))) ? <Required /> : ''}
                    </>
                )
            case 'Submit':
                return (
                    <StyledInput
                        id = {containing}
                        type = 'submit'
                        value = 'Submit'
                    />
                )
            case 'Name' :
                return (
                    <>
                        <StyledInput
                            id = {containing}
                            type = "text"
                            placeholder = {containing}
                            onChange = {e => setState({name: e.target.value})}
                        />
                        {(props.submit && (state.name === '')) ? <Required /> : ''}
                    </>
                )
            default:
                return
        }
    }

    const sendData = () => {

        switch (containing) {    //different function depends on input
            case 'Dish' :
                props.saveDish(state.dish)
                break;
            case 'Name' :
                props.saveName(state.name);
                break;
            case 'Preparation Time' :
                props.savePreparationTime(state.time)
                break;
            default:
                break;
        }
    }

    useEffect(() => {  //useEffect from React Hooks
        sendData()
    }, [state.name, state.time, state.dish])

    return (
        <InputWrapper>
            {containing === 'Submit' ? '' : <StyledLabel htmlFor={containing}>{containing}</StyledLabel>}
            {inputType(containing)}
        </InputWrapper>
    )
}

const mapStateToProps = state => {
    return {
        name: state.name,
        time: state.time,
        dish: state.dish,
        submit: state.submit
    }
}

const mapDispatchToProps = {
    saveName,
    savePreparationTime,
    saveDish
}

export default connect(mapStateToProps, mapDispatchToProps)(Input)