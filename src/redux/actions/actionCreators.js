import { SAVE_NAME, SAVE_PREPARATION_TIME, SAVE_DISH, SAVE_NO_OF_SLICES, SAVE_DIAMETER, SAVE_SPICINESS_SCALE, SAVE_SLICES_OF_BREAD, SET_SUBMIT } from "./actionTypes"

export const saveName = payload => {
    return({
        type: SAVE_NAME,
        payload
    })
}

export const savePreparationTime = payload => {
    return({
        type: SAVE_PREPARATION_TIME,
        payload
    })
}

export const saveDish = payload => {
    return({
        type: SAVE_DISH,
        payload,
    })
}

export const saveNoOfSlices = payload => {
    return({
        type: SAVE_NO_OF_SLICES,
        payload,
    })
}

export const saveDiameter = payload => {
    return({
        type: SAVE_DIAMETER,
        payload,
    })
}

export const saveSpicinessScale = payload => {
    return({
        type: SAVE_SPICINESS_SCALE,
        payload,
    })
}

export const saveSlicesOfBread = payload => {
    return({
        type: SAVE_SLICES_OF_BREAD,
        payload,
    })
}

export const setSubmit = payload => ({
    type: SET_SUBMIT,
    payload,
})