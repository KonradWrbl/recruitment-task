# Form - recruitment task
Form for ordering a meal created with ReactJS, Redux and Styled-Components.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Deployment](#Deployment)

## General Info
This project has been created to make ordering meals easier for clients. Adequate technolgies have been used to make this app intuitive and clear.

## Technologies
Project is created with:
* JavaScript;
* ReactJS;
* Redux, React-Redux;
* Styled-Components, CSS in JS;

## Setup

In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

## Deployment
This app can be find here: [https://5d9327c5c1064b0009ca8a3c--youthful-wozniak-f92848.netlify.com](https://5d9327c5c1064b0009ca8a3c--youthful-wozniak-f92848.netlify.com).
